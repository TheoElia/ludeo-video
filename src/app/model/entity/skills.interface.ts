/**
 * @module model
 * An interface that represents skill of workers, equipments and the differents parts of a task
 */
export interface ISkills {
	story : number;
	discipline : number;
	debugging : number;
	graphics : number;
	sound : number;
	programming : number;
	world_design : number;
	game_design : number;
	research : number;
}
