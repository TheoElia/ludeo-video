import { I18nString } from '../util/translation/i18nString.class';
import { Skillset } from './skillset.class';
/**
 * Class which represents equipment player can buy in the game.
 * groupEquipment and individualEquipment inherit from it
 */
export abstract class Equipment {

	private _name : I18nString;
	private _description : I18nString;

	private _buyingPrice : number;
	private _sellingPrice : number;

	private _skillset : Skillset;
	/**
	 * This attribute represent the comfort the equipment gives
	 */
	private _comfort : number;

	/**
	 * Constructor, intialize the Object with a skillset and a comfort value
	 * By default 0 to everystats
	 * @param skillset : The skillset of the equipment
	 * @param comfort : The comfort the equipemnt gives
	 */
	constructor(name : I18nString, description : I18nString, skillset : Skillset = new Skillset(), comfort : number = 0 ) {
		this._skillset = skillset;
		this._comfort = comfort;
	}

	// Public Methods
	/* Getter and setters */

	public get name(): I18nString {
		return this._name;
	}

	public set name(value: I18nString) {
		this._name = value;
	}

	public get description(): I18nString {
		return this._description;
	}

	public set description(value: I18nString) {
		this._description = value;
	}

	public get buyingPrice(): number {
		return this._buyingPrice;
	}

	public set buyingPrice(value: number) {
		this._buyingPrice = value;
	}

	public get sellingPrice(): number {
		return this._sellingPrice;
	}

	public set sellingPrice(value: number) {
		this._sellingPrice = value;
	}

	public get skillset(): Skillset {
		return this._skillset;
	}

	public set skillset(value: Skillset) {
		this._skillset = value;
	}

	public get comfort(): number {
		return this._skillset._story;
	}

	public set comfort(value: number) {
		if (value >= 0) {
			this._skillset._story = value;
		}
	}
	public get story(): number {
		return this._skillset._story;
	}

	public set story(value: number) {
		if (value >= 0) {
			this._skillset._story = value;
		}
	}

	public get discipline(): number {
		return this._skillset._discipline;
	}

	public set discipline(value: number) {
		if (value >= 0) {
			this._skillset._discipline = value;
		}
	}

	public get debugging(): number {
		return this._skillset._debugging;
	}

	public set debugging(value: number) {
		if (value >= 0) {
			this._skillset._debugging = value;
		}
	}

	public get graphics(): number {
		return this._skillset._graphics;
	}

	public set graphics(value: number) {
		if (value >= 0) {
			this._skillset._graphics = value;
		}
	}

	public get sound(): number {
		return this._skillset._sound;
	}

	public set sound(value: number) {
		if (value >= 0) {
			this._skillset._sound = value;
		}
	}

	public get programming(): number {
		return this._skillset._programming;
	}

	public set programming(value: number) {
		if (value >= 0) {
			this._skillset._programming = value;
		}
	}

	public get world_design(): number {
		return this._skillset._world_design;
	}

	public set world_design(value: number) {
		if (value >= 0) {
			this._skillset._world_design = value;
		}
	}

	public get game_design(): number {
		return this._skillset._game_design;
	}

	public set game_design(value: number) {
		if (value >= 0) {
			this._skillset._game_design = value;
		}
	}

	public get research(): number {
		return this._skillset._research;
	}

	public set research(value: number) {
		if (value >= 0) {
			this._skillset._research = value;
		}
	}

}
