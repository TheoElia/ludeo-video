import { ISkills } from 'model/entity/skills.interface';
import { Skillset } from 'model/entity/skillset.class';
import { Component } from '@angular/core';
import { Calendar } from 'model/entity/calendar.class';
import { I18nString } from 'model/util/translation/i18nString.class';
import { TranslationService } from 'model/util/translation/translation-service.class';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	title = 'Jeu';
	trad = TranslationService.shared;
	//title = new I18nString('GENERAL-TITLE_GAME').toString();
	public onInit() {
		console.log('Coucou');
		console.log(this.title);
	}
}
