import { ISkills } from './skills.interface';
import { I18nString } from '../util/translation/i18nString.class';

export class Skillset implements ISkills {

	_story : number;
	_discipline : number;
	_debugging : number;
	_graphics : number;
	_sound : number;
	_programming : number;
	_world_design : number;
	_game_design : number;
	_research : number;

	/* Constructors */
	constructor(skills : ISkills) {
		this.story = skills.story;
		this.discipline = skills.discipline;
		this.debugging = skills.debugging;
		this.sound = skills.sound;
		this.graphics = skills.graphics;
		this.programming = skills.programming;
		this.world_design = skills.world_design;
		this.game_design = skills.world_design;
		this.research = skills.research;
	}
	/* Public Methods */

	/**
	* A method that return ISkills, can be use to copy stats from one entity to one another
	* @Return ISkills
	*/
	public toJSON() : ISkills {
		return {
			story : this.story,
			discipline : this.discipline,
			debugging : this.debugging,
			graphics : this.graphics,
			sound : this.sound,
			programming : this.programming,
			world_design : this.world_design,
			game_design : this.world_design,
			research : this.research
		};
	}

	/**
	* A method which return the String of a JSON, used for debugging mainly or writing in files
	* @return String
	*/
	public ToJSONString() : string {
		return JSON.stringify(this.toJSON());
	}

	/* Getter and setters */

	public get story(): number {
		return this._story;
	}

	public set story(value: number) {
		this._story = value;
	}

	public get discipline(): number {
		return this._discipline;
	}

	public set discipline(value: number) {
		this._discipline = value;
	}

	public get debugging(): number {
		return this._debugging;
	}

	public set debugging(value: number) {
		this._debugging = value;
	}

	public get graphics(): number {
		return this._graphics;
	}

	public set graphics(value: number) {
		this._graphics = value;
	}

	public get sound(): number {
		return this._sound;
	}

	public set sound(value: number) {
			this._sound = value;
	}

	public get programming(): number {
		return this._programming;
	}

	public set programming(value: number) {
			this._programming = value;
	}

	public get world_design(): number {
		return this._world_design;
	}

	public set world_design(value: number) {
		this._world_design = value;
	}

	public get game_design(): number {
		return this._game_design;
	}

	public set game_design(value: number) {
		this._game_design = value;
	}

	public get research(): number {
		return this._research;
	}

	public set research(value: number) {
		this._research = value;
	}

}
