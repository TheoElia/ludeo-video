import {Worker} from 'app/model/entity/worker.class';
import {I18nString} from 'app/model/util/translation/i18nString.class';
import {Skillset} from 'app/model/entity/skillset.class';

describe('Worker', () => {

	it('Should have default stats', () => {
		const worker = new Worker();
		expect(worker.energy).toEqual(1, 'When created, energy must be equal to 1');
		expect(worker.energyState).toEqual(new I18nString('MODEL-WORKER-ENERGY_STATE-NORMAL'), 'When created, energyState must be NORMAL');
		console.log(worker);
	});

	it('Should have initialized values', () => {
		const worker = new Worker("Bobby", new Skillset( {story : 3, discipline : 3, debugging : 3, graphics : 3, sound : 3, programming : 3, world_design : 3, game_design : 3, research : 3}), 0);
		expect(worker.name).toEqual("Bobby", 'Name must be "Bobby"');
		expect(worker.skillset).toEqual(new Skillset({story : 3, discipline : 3, debugging : 3, graphics : 3, sound : 3, programming : 3, world_design : 3, game_design : 3, research : 3}), 'Skillset initialized to 3');
	});

	// it('Should increase skill value when object equiped', () =>{
	//	 const worker = new Worker();
	//	 const equipement = new Equipement();
	});
});