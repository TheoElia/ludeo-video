import {I18nString} from '../util/translation/i18nString.class';
import { NameWorker } from './name-worker.interface';
import { IndividualEquipment } from './individual-equipment.class';
import { worker } from 'cluster';
import { Skillset } from './skillset.class';
/**
 * @module model/worker
 */
export class Worker {

	// The amount of energy a worker begins with
	static readonly MAX_ENERGY : number = 1;
	// The amount of energy where a worker is deemed tired
	static readonly TIRED_TRESHOLD : number = 0.4;
	// The amount of energy where a worker is deemed exahausted
	static readonly EXHAUSTED_TRESHOLD : number = 0.2;
	// The amount of energy where a worker is deemed surhuman
	static readonly SUPERHUMAN_TRESHOLD : number = 1.2;

	// The skills of the worker
	private _skillset : Skillset; // The Skills of the worker

	private _name : NameWorker;

	private _salary : number;

	private _energy : number = 1;

	private _equipments : IndividualEquipment[];

	constructor(name : NameWorker, skillset : Skillset, salary : number, equipment : IndividualEquipment[] = [] ) {
		this._name = name;
		this._skillset = skillset;
		this._salary = salary;
		this._equipments = equipment;
	}


	public get skillset(): Skillset {
		return this._skillset;
	}

	public set skillset(value: Skillset) {
		this._skillset = value;
	}

	public get name(): NameWorker {
		return this._name;
	}

	public set name(value: NameWorker) {
		this._name = value;
	}

	public get salary(): number {
		return this._salary;
	}

	public set salary(value: number) {
		this._salary = value;
	}

	public get energy() : number  {
		return this._energy;
	}

	public set energy(value: number ) {
		this._energy = value;
	}

	public get energyState() : I18nString {
		if (this._energy > Worker.SUPERHUMAN_TRESHOLD) {
			return new I18nString('MODEL-WORKER-ENERGY_STATE-SUPERHUMAN_TRESHOLD');
		}
		else if (this._energy < Worker.EXHAUSTED_TRESHOLD) {
			return new I18nString('MODEL-WORKER-ENERGY_STATE-EXHAUSTED_TRESHOLD');
		}
		else if (this._energy < Worker.TIRED_TRESHOLD) {
			return new I18nString('MODEL-WORKER-ENERGY_STATE-TIRED_TRESHOLD');
		}
		else {
			return new I18nString('MODEL-WORKER-ENERGY_STATE-NORMAL');
		}
	}
	public get equipments(): IndividualEquipment[] {
		return this._equipments;
	}

	public set equipments(value: IndividualEquipment[]) {
		this._equipments = value;
	}

}
