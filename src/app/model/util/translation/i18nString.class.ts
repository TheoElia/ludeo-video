/*
 * This class is used to store i18n String.
 * It will be translated to the actual locale by the Angular service i18next
 */
import { TranslationService } from 'model/util/translation/translation-service.class';

export class I18nString {

	/* Private Attribute */
	private _i18nString : string;

	/* Constructor */
	constructor(I18nString : string) {
		this._i18nString = I18nString;
	}

	public get I18nString() : string {
		return this._i18nString;
	}

	public toString() : string {
		return TranslationService.shared.translate(this);
	}
}
