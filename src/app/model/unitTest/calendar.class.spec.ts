import {Calendar} from 'app/model/entity/calendar.class';

describe('Calendar', () => {
	const YEAR_TICK_0 = Calendar.YEAR_TICK_0;
	const TICKS_IN_DAY = Calendar.TICKS_IN_DAY;

	it('Should begin with the year ' + YEAR_TICK_0 + ' awith ticks 0', () => {
		const calendar = new Calendar();
		console.log(calendar.toString());
		expect(calendar.year).toEqual(YEAR_TICK_0, 'When total ticks equals 0, the year must be ' + YEAR_TICK_0);
		expect(calendar.totalTicks).toEqual(0, 'Total ticks must be 0');
		expect(calendar.day).toEqual(1, 'When total ticks is 0 quarter month must be 1');
		expect(calendar.monthNumber).toEqual(1, 'When total ticks is 0 quarter month must be 1');
		expect(calendar.week).toEqual(1, 'When total ticks is 0 week number must be 1');
	});

	it('Should increase the tick when nextTick() is called', () => {
		const calendar = new Calendar();
		let beforeTicks, afterTicks;

		beforeTicks = calendar.totalTicks;
		calendar.nextTick();
		afterTicks = calendar.totalTicks;
		expect(afterTicks - beforeTicks).toEqual(1);
	});

	it('Should increase the number of quarter weeks every ' + TICKS_IN_DAY + ' ticks from 1 to ' + Calendar.DAYS_IN_A_WEEK, () => {
		const calendar = new Calendar();
		for (let i = 1; i < Calendar.DAYS_IN_A_WEEK + 1 ; i++) {
			calendar.nextDay();
			expect(calendar.day).toEqual((i % Calendar.DAYS_IN_A_WEEK) + 1);
		}
	});

	it('Should increase the number of weeks every ' + Calendar.DAYS_IN_A_WEEK + ' ticks from 1 to ' + Calendar.WEEKS_IN_A_MONTH, () => {
		const calendar = new Calendar();
		for ( let i = 1; i < Calendar.WEEKS_IN_A_MONTH + 1 ; i++ ) {
			calendar.nextWeek();
			expect(calendar.week).toEqual((i % Calendar.WEEKS_IN_A_MONTH) + 1);
		}
	});

	it('Should increase the number of months every ' + Calendar.WEEKS_IN_A_MONTH + ' weeks from 1 to ' + Calendar.MONTHS_IN_A_YEAR, () => {
		const calendar = new Calendar();
		for ( let i = 0; i <= Calendar.MONTHS_IN_A_YEAR + 1 ; i++ ) {
			console.log(calendar.toString());
			console.log(calendar.monthNumber + ' ' + i);
			console.log(i % Calendar.MONTHS_IN_A_YEAR);
			expect(calendar.monthNumber).toEqual(i % Calendar.MONTHS_IN_A_YEAR + 1);
			calendar.nextMonth();
		}
	});
});
