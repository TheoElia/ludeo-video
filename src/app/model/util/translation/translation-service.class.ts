const i18next : any = require('i18next');
const XHR : any = require('i18next-xhr-backend');

import { I18nString } from 'model/util/translation/i18nString.class';

/**
 * @module translation
 */
export class TranslationService {
	static _shared = new TranslationService();

	private isLoaded : boolean;

	private constructor() {
		this.isLoaded = false;

		i18next
			.use(XHR)
			.init({
				lng : 'fr-FR',
				nsSeparator: false,
				keySeparator: false,
				backend : {
					loadPath : 'app/locales/fr/translation.json'
				}
			},
			(err, t) => {
				if (err) {
					console.log(err);
					throw(err);
				}
				console.log('TRAD : ' + i18next.t('JANUARY'));
				this.isLoaded = true;
			});

	}
	public isTranslationServiceLoaded() : boolean {
		return this.isLoaded;
	}

	public translate(i18nCode : I18nString ) : string {

		if (this.isTranslationServiceLoaded) {
			return i18next.t(i18nCode);
		}
		else {
			throw Error('Translation service not ready');
		}
	}

	static get shared() {
		return TranslationService._shared;
	}
}
