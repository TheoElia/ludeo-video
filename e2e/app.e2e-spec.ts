import { GameDevTycoonPlusPlusPage } from './app.po';

describe('game-dev-tycoon-plus-plus App', () => {
	let page: GameDevTycoonPlusPlusPage;

	beforeEach(() => {
		page = new GameDevTycoonPlusPlusPage();
	});

	it('should display message saying app works', () => {
		page.navigateTo();
		expect(page.getParagraphText()).toEqual('app works!');
	});
});
