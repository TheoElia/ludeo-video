import { I18nString } from '../util/translation/i18nString.class';
/**
 * @module model
 * A Class which manages the calendar.
 * The Calendar register a number of tick and convert it in the In-game Calendar.
 * The Controller can call the calendar to go to the next tick
 * Time 0 is 1 January 1980
 * Divided in Ticks/Day/Week/Month/Year
 */
export class Calendar {

	// The Quarter week is the smallest lapse of time for the player.
	static readonly TICKS_IN_DAY : number = 10;
	static readonly DAYS_IN_A_WEEK : number = 5;
	static readonly WEEKS_IN_A_MONTH : number = 4;
	static readonly MONTHS_IN_A_YEAR : number = 12;
	static readonly YEAR_TICK_0 : number = 1980;

	// Array with month
	static readonly MONTH : I18nString[] = [
		new I18nString('JANUARY'),
		new I18nString('FEBRUARY'),
		new I18nString('MARCH'),
		new I18nString('APRIL'),
		new I18nString('MAY'),
		new I18nString('JUNE'),
		new I18nString('JULY'),
		new I18nString('AUGUST'),
		new I18nString('SEPTEMBER'),
		new I18nString('OCTOBER'),
		new I18nString('NOVEMBER'),
		new I18nString('DECEMBER')
	];

	/* Private Attributes */

	// The amount of time (in Tick) past since the initialization
	private timePastSinceStart : number;
	// The time (in tick) when the calendar began
	private readonly offSet : number;

	// Public Methods

	// Constructor
	constructor(offSet : number = 0) {
		this.offSet = offSet;
		this.timePastSinceStart = 0;
	}

	public toString() : string {
		return 'Tick n°' + this.totalTicks + ' ' + this.day + ' Days ' + this.week + ' Week ' + this.month.I18nString + ' ' + this.year;
	}

	/**
	 * The Calendar advance to nbTicks ticks
	 * @param nbTicks The number of tick to advance
	 * @returns Nothing
	 */
	public nextTick(nbTick : number = 1) : void {
		this.timePastSinceStart = this.timePastSinceStart + nbTick;
	}
	/**
	 * The Calendar advance to nbDay days
	 * @param nbDay The number of quarterweek to advance
	 * @returns Nothing
	 */
	public nextDay(nbDay : number = 1) : void {
		this.nextTick(Calendar.TICKS_IN_DAY * nbDay);
	}

	/**
	 * The Calendar advance to nbweek weeks
	 * @param nbweek The number of week to advance
	 * @returns Nothing
	 */
	public nextWeek(nbWeek = 1) : void {
		this.nextDay(Calendar.DAYS_IN_A_WEEK * nbWeek);
	}

	/**
	 * The Calendar advance to nbmonth months
	 * @param nbmonth The number of month to advance
	 * @returns Nothing
	 */
	public nextMonth(nbMonth = 1) : void {
		this.nextWeek(Calendar.WEEKS_IN_A_MONTH * nbMonth);
	}

	/**
	 * The Calendar advance to nbyear years
	 * @param nbyear The number of year to advance
	 * @returns Nothing
	 */
	public nextYear(nbyear = 1) : void {
		this.nextMonth(Calendar.MONTHS_IN_A_YEAR);
		console.log('Tick tick next year');
	}

	public get totalTicks() : number {
		return this.offSet + this.timePastSinceStart;
	}

	public get totalDays() : number {
		return Math.trunc(this.totalTicks / Calendar.TICKS_IN_DAY);
	}

	public get totalWeeks() : number {
		return Math.trunc(this.totalDays / Calendar.DAYS_IN_A_WEEK);
	}

	public get totalMonths() : number {
		return Math.trunc(this.totalWeeks / Calendar.WEEKS_IN_A_MONTH);
	}

	public get totalYears() : number {
		return Math.trunc(this.totalMonths / Calendar.MONTHS_IN_A_YEAR);
	}

	public get year() : number {
		return Calendar.YEAR_TICK_0 + this.totalYears;
	}
	/**
	 * @returns Month number between [1,12]
	 */
	public get monthNumber() : number {
		return ( this.totalMonths % Calendar.MONTHS_IN_A_YEAR ) + 1;
	}
	/**
	 *@returns String of the current month
	*/
	public get month() : I18nString {
		return Calendar.MONTH[this.monthNumber - 1];
	}
	/**
	 * @returns the current week in the current month. An int between [1,4]
	 */
	public get week() : number {
		return ( this.totalWeeks % Calendar.WEEKS_IN_A_MONTH ) + 1;
	}
	/**
	 * @returns The current day of the week, An int between [1,4]
	 */
	public get day() : number {
		return ( this.totalDays % Calendar.DAYS_IN_A_WEEK ) + 1;
	}

}
